from math import e

def U(n):
    return (1+1/n)**n

def approxim_e(p,U):
    n = 1
    while e - U(n)>10**(-p):
        n += 1
    return n, U(n)

for p in range(1,10):
    print(p,approxim_e(p,U))