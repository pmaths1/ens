from math import e

def U(n):
    return (1+1/n)**n

def evolution_u(p,U):
    n = 1
    while U(n+1)-U(n)>10**(-p):
        n += 1
    return n, U(n)
