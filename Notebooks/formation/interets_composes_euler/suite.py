from mpmath import *
mp.dps=10000

def U(n):
    return power(fadd(1,fdiv(1,n)),n)

p=int(input("Nombre de chiffre après la virgule voulu : "))
k=int(input("Ecart avec e à 10^(-k) près k = "))

n=1
while fsub(U(n+1),U(n))>power(10,-k) :
    n=n+1
    
print("U(",n,") = ",nstr(U(n),p))
print("e =",nstr(e,p))


