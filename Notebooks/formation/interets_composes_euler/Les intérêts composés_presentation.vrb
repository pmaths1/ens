\onslide<1>
    \begin{code}\small
        \lstinputlisting[language=Python,columns=fullflexible]{suite2.py}
    \end{code}

    \begin{columns}

      \begin{column}{0.5\textwidth}
                 \begin{overprint}
          \begin{enumerate}
              \item<only1> Modifier le programme précédent pour valeur approchée du nombre \textbf{e} par $U\left(n\right)$ à moins de $10^{-p}$ .
              \item<only2-7> Déterminer les rangs nécessaires pour avoir la précision voulue:
                      \renewcommand*{\arraystretch}{1.5}
                    \begin{tabular}{|c|c|}
                        \hline
                        Précision $10^{-p}$ pour $p=$  & Rang $n=$ \\
                        \hline
                        1 & 13 \\
                        \hline
                        2 & 135   \\
                        \hline
                        3 &  1359   \\
                        \hline
                        4 & 13591  \\
                        \hline
                        5 &  135913    \\
                        \hline
                    \end{tabular}
              \item<only8-11> Comparer les progressions de ces deux suites de nombres $p$ et $n$.
                Faire une conjecture.
                  \only<9-11>{
                      \begin{block}{conjecture}
                          À chaque fois qu'on progresse de 1 pour la précision (1chiffre supplémentaire de juste), on doit effectuer environ 10 fois plus de boucles.
                      \end{block}
                  }
              \item<only10-11>  En déduire un ordre de grandeur du rang $n$ correspondant à une précision de $10^{-10}$.
                  \only<11>{\alert{$n$ doit être proche de 10 milliards !! }}

              \item<only12-14>  Votre programme confirme-t-il votre conjecture?
%                         \only<13-14>{
%                        \begin{code}\small
%                            \begin{lstlisting}[language=Python,columns=fullflexible]
%>>> print(approxim_e(10,U))
%>>> (66953935, 2.7182818283597854)
%                            \end{lstlisting}
%                        \end{code}
%                    }
%                        \only<14>{66 953 935 c'est à dire environ 67 millions, notre conjecture s'avère fausse. }

              \item<only16-19>  Déterminer les rangs nécessaires pour avoir la précision voulue~:
                  \renewcommand*{\arraystretch}{1.5}
                  \begin{tabular}{|c|c|}
                      \hline
                      Précision $10^{-p}$ pour $p=$  & Rang $n=$ \\
                      \hline
                      1 &  \only<17->{3}   \\
                      \hline
                      2 & \only<18->{11}    \\
                      \hline
                      3 &  \only<19->{36}   \\
                      \hline
                      4 &  \only<19->{116}    \\
                      \hline
                      5 &  \only<19->{368}    \\
                      \hline
                  \end{tabular}
              \item<only20> Peut-on faire une conjecture~? est-elle vérifiée par le programme~?
          \end{enumerate}
      \end{overprint}
      \end{column}
\end{columns}
