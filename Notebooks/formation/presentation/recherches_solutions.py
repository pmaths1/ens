#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 18:49:41 2021

@author: Pascal Malingrey
formation 19 janv. 2021
"""
#%%

def f(x):
    return x**3-3*x+1

def balayage1(a,N,f):
    x = a 
    while f(a)*f(x)>0: 
        x = x + 10**(-N)
    return x

def balayage2(a,N,f):
    for i in range(N):
        x = a 
        while f(a)*f(x)>0:
            x = x + 10**(-i)
        x = x - 10**(-i)
    return x

def dichotomie(a,b,N,f):
    while b-a>10**(-N):
        m = (a+b)/2 
        if f(a)*f(m)>0 :
            a = m
        else:
            b = m
    return m

def Lagrange(a,b,N,f):
    m = a - (b-a)/(f(b)-f(a))*f(a)
    while m-a>10**(-N): 
        if f(a)*f(m)>0 :
            m = a
        else:
            m = b
    return m

#%%
     
import time 
import matplotlib.pyplot as plt
from matplotlib import style
style.use('fivethirtyeight')

a,b,N = -2,-1,5
tpsdep = time.process_time() 
print(balayage1(a,N,f))
tps1 = time.process_time() - tpsdep 

tpsdep = time.process_time() 
print(balayage2(a,N,f))
tps2 = time.process_time() - tpsdep 

tpsdep = time.process_time()
print(dichotomie(a,b,N,f))
tps3 = time.process_time() - tpsdep 

print("le temps mis en seconde par la méthode 1 est ",tps1)
print("le temps mis en seconde par la méthode 2 est ",tps2)
print("le temps mis en seconde par la méthode 3 est ",tps3)

val = [tps1,tps2,tps3]    # the bar lengths
pos = [1,2,3]    # the bar centers on the y axis
legende = ("Méthode 1", "Méthode 2", "Méthode 3")
plt.barh(pos,val, align='center',facecolor='#9999ff', edgecolor='white')


plt.title('Temps des 3 méthodes')
plt.grid()
plt.show()

