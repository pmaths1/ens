#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 12 19:37:56 2021

@author: Pascal Malingrey
"""

from matplotlib import pyplot as plt
from math import atan
import numpy as np

# une fonction axes permet de créer les axes comme désirés
def axes(graph):
    graph.spines['left'].set_position('zero')          
    graph.spines['right'].set_color('none')
    graph.spines['bottom'].set_position('zero')
    graph.spines['top'].set_color('none')



#%%
def repr_affine(f,gen,st =".r", mrep = False):
    """
    Parameters
    ----------
    f : fonction solution
    gen : points générer par la méthode Euler
    st: stype pour la représentation de gen
    """
    xmin = min(gen[0])
    xmax = max(gen[0])+ 0.2
    
    fig =  plt.figure()
    graph =  fig.add_subplot(1,1,1)
    
    if f is not None:
        abs_x = np.linspace(xmin,xmax,30)
        # On calcule l'ensembre des images 
        ord_y = f(abs_x)
        graph.plot(abs_x,ord_y,':')
    
    graph.plot(gen[0],gen[1],st)
    graph.axis('equal')
    if len(gen[0])<5:
        for i in range(len(gen[0])):
            plt.annotate('A'+str(i), # this is the text
                 (gen[0][i], gen[1][i]), # this is the point to label
                 textcoords="offset points", # how to position the text
                 xytext=(0,5), # distance from text to points (x,y)
                 ha='center')
    if mrep: axes(graph)
    plt.grid()
    plt.show()
    return None

#%%
def f(x):
    return np.arctan(x)

def df(x):
    return 1/(1+x**2)


def gen_coord(a:float, y:float, h:float,nb:int, df):
    """
    Parameters
    ----------
    y : float
        f(a)
    df : function pour calculer
        f'(a)
    a : float
        abscisse de départ
    h : float
        DESCRIPTION.
    n: int
        nombre de points à construire
    Returns
    -------
    """
    xc = [a+i*h for i in range(nb+1)]
    yc = [y]
    for i in range(nb):
        yc.append(yc[-1] + df(xc[i])*h )
    return xc,yc

assert gen_coord(0, 0, 0.5, 2, lambda x:1/(0.5+x) ) == ([0,0.5,1 ],[0,1,1.5]), gen_coord(0, 0, 0.5, 2, lambda x:1/(x+0.5) )

def graph_q1(st =".r", mrep = False):
    generateur = gen_coord(0, 0, 0.5, 1, df)
    return repr_affine(f,generateur,st,mrep)

def graph_q2(st =".r", mrep = False):
    generateur = gen_coord(0, 0, 0.5, 2, df)
    return repr_affine(f,generateur,st,mrep)

def graph_q3(st =".r", mrep = False):
    generateur = gen_coord(0, 0, 0.5, 10, df)
    return repr_affine(f,generateur,st,mrep)

def graph_q4(n, st =".r", mrep = False):
    h = 5/n
    generateur = gen_coord(0, 0, h, n, df)
    return repr_affine(f,generateur,st,mrep)

#%%
def f2(x):
    return (1-np.exp(2*x))/(1+np.exp(2*x))

def df2(y):
    return y**2 - 1

def gen_coord2(a:float, y:float,  h:float, nb:int, df):
    """
    Parameters
    ----------
    y : float
        f(a)
    df : function pour calculer
        f'(a) pour équation du type y'=f(y)
    a : float
        abscisse de départ
    h : float
        DESCRIPTION.
    n: int
        nombre de points à construire
    Returns
    -------
    """
    xc = [a+i*h for i in range(nb+1)]
    yc = [y]
    for i in range(nb):
        yc.append(yc[-1] + df(yc[-1])*h )
    return xc,yc

def graph_q5(n, st =".r", mrep = False):
    h = 5/n
    generateur = gen_coord2(0, 0, h, n, df2)
    return repr_affine(f2,generateur,st,mrep)


#%%

def df3(y):
    return np.sin(y)


def graph_q6(n, st ="r", mrep = False):
    h = 6/n
    gp = gen_coord2(0, 0.5, h, n, df3)
    gmx, gmy = gen_coord2(0, 0.5, -h, n, df3)
    gmx.reverse()
    gmy.reverse()
    gm = gmx,  gmy
    generateur = ( gm[0] + gp[0], gm[1] + gp[1])
    return repr_affine(None,generateur,st,mrep=True)