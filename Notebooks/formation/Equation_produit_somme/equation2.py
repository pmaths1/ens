def sol_equation(K):
    solutions = []
    for n in range(K+1,2*K+1):
        for m in range(n,K*n):
            if (n-K)*m-K*n == 0:
                solutions.append([n,m])
    return solutions

