#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 16 15:54:06 2021

@author: pascal
"""

from random import random
from math import sqrt
from matplotlib.widgets import Cursor
import matplotlib.pyplot as plt
import numpy as np

def MonteCarlo(nbrpoints):
    compteur=0
    for i in range(nbrpoints):
        x=random()
        y=random()
        distance=sqrt(x**2+y**2)
        if (distance<1):
            compteur=compteur+1
    return compteur/nbrpoints



def monte_carlo(n_points):
    # Définition de 4 listes pour les coordonnées des points intérieurs et extérieurs
    interieur = 0
    xin, yin, xout, yout = [[] for _ in range(4)] 

    for _ in range(n_points):
        x = random()
        y = random()
        if x**2+y**2 <= 1:
            interieur += 1
            xin.append(x)
            yin.append(y)
        else:
            xout.append(x)
            yout.append(y)

    #print ("Une estimation de pi est pour N = %d is %.4f" %(n_points, 4*interieur/n_points))
    
    x = np.linspace(0,1,1000)
    y = np.sqrt(1-x**2)
    
    fig, ax = plt.subplots()
    ax.set_aspect('equal') # repère orthonormé
    ax.scatter(xin, yin, color='g', marker='o', s=4)
    ax.scatter(xout, yout, color='r', marker='o', s=4)
    ax.plot(x,y,color='b')
    plt.show()
    
francais = [942,102,264,339,1587,95,104,77,841,89,0,534,324,715,514,286,106,646,790,726,624,215,0,30,24,32]
frequenceF = [ effectif/100 for effectif in francais]