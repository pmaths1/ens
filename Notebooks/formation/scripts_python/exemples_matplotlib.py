#!/usr/bin/env python3

from matplotlib import pyplot as plt
import numpy as np

#%%
plt.plot([-1,2,3],[2,4,-1],'b')
plt.show()

#%%
def g(x):
    return x**2-1

xmin= -2
xmax= 5

abs_x = np.linspace(xmin,xmax,50)
# On calcule l'ensembre des images 
ord_y = g(abs_x)

fig =  plt.figure()
graph =  fig.add_subplot(1,1,1)
graph.plot(abs_x,ord_y)
graph.fill_between(abs_x,ord_y,color='C0', alpha=0.3)
graph.spines['left'].set_position('zero')          
graph.spines['right'].set_color('none')
graph.spines['bottom'].set_position('zero')
graph.spines['top'].set_color('none')
plt.grid()

#%%---------------------------------------------------------------


def f(x):
    return np.cos(x)

def g(x):
    return np.sin(x)

xmin= -1
xmax= 6

abs_x = np.linspace(xmin,xmax,50)
# On calcule l'ensembre des images 
ord_y1 = f(abs_x)
ord_y2 = g(abs_x)
fig =  plt.figure()
graph =  fig.add_subplot(1,1,1)
graph.plot(abs_x,ord_y1)
graph.plot(abs_x,ord_y2)
graph.fill_between(abs_x,ord_y1,ord_y2,where=ord_y2>=ord_y1, color='C0', alpha=0.3)
graph.spines['left'].set_position('zero')          
graph.spines['right'].set_color('none')
graph.spines['bottom'].set_position('zero')
graph.spines['top'].set_color('none')
plt.grid()