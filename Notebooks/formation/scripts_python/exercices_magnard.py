#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan  7 18:42:30 2021

@author: Pascal Malingrey
formation 19 janv. 2021
"""



#%% PAGE 83

from math import cos,pi
def dichotomie(n):
   a = 0.5
   b = pi/2+0.5
   while abs(b-a) > 1/(10**n):
      c =(a+b)/2
      if 3*cos(2*c-1) > 0 :
         a = c
      else :
         b = c
   print("Une valeur approchée de x est comprise entre",a,"et",b)
   

#%% PAGE 361

def entrer_entier(a=""):
   verification=0
   while verification==0:
      x=input("Entrer un nombre entier")
      try:
         x=int(x)
         verification=1
         return(x)
      except:
         verification=0
         
def factorielle(n):
   x=1
   if n<1:
       return(1)
   else:
      for i in range(n):
         x=x*(i+1)
      return(x)

def combinaison(n,p):
   x=(factorielle(n))/(factorielle(p)*factorielle(n-p))
   x=int(x)
   return(x)

def triangle_pascal(n):
   for i in range(n):
      for j in range(i+1):
         x=combinaison(i,j)
         print(x, end=" ")
      print()

#n=entrer_entier("n")
#triangle_pascal(n)

#%% P81

from random import randint

def binomiale(n, p):
    L=[bernoulli(p) for i in range(n)]
    return sum(L)


#%%
n=10
L=[[0]*n for i in range(n)]
#[[0]*n]*n


#%% pas trop d'intérêt
def f(x):
    if x<1:
        return x+1
    if x >1:
        return -x**2+4*x+1
    return 3

def f1(x):
    return (x+1)*(x<1)+ (-x**2+4*x+1)*(x>1)+ 3*(x==1)
#%%

from random import randint

def Paires(L):
    n = len(L)
    P = [0,0]
    for i in range (1,n):
        P[0] = L[i-1]
        for k in range(i+1,n+1):
            P[1]=L[k-1]
            print(P)
    return
    
#%%

def Paires1(L):
    n = len(L)
    P = [0,0]
    for i in range (0,n-1):
        P[0] = L[i]
        for k in range(i+1,n):
            P[1]=L[k]
            print(P)
    return

#%%
def Paires(L):
    n = len(L)
    P = [0,0]
    liste = []
    for i in range (0,n-1):
        P[0] = L[i]
        for k in range(i+1,n):
            P[1]=L[k]
            liste.append(P.copy())
    return liste

#%%
def Paires2(L):
    return [[L[i],j] for i in range(len(L)) for j in L[(i+1):]]

Paires2([1,2,3])

#%%
import random

L=[random.randint(2,i+3) for i in range(1,4)]
L

#%%
[[m,n] for m in range(-10,11) for n in range(-10,11) \
  if (m*n)**2+16*(m-1)*(n-1)+4*m*n == 0]

#%% " PAGE 353 "

def permutliste(seq):
    p = [seq]
    n = len(seq)
    for k in range(0,n-1):
        for i in range(0,len(p)):
            z = p[i][:]
            for c in range(0,n-k-1):
                z.append(z.pop(k))
                p.append(z[:])
    return p
  
def permutchaine(ch):
    return[''.join(z) for z in permutliste(list(ch))]
 
   
#%%
L = [1,2,3,4]

            
def permutlisteP(seq):
    p = [seq]
    n = len(seq)
    for k in range(n-1):
        for i in range(len(p)):
            z = p[i].copy() #faire une copie, afin de ne pas modifier p[i]
            for _ in range(n-k-1):
                # prend élément à la position k pour le mettre à la fin
                z.append(z.pop(k))
                # ajouter la nouvelle permutation à la liste des précédentes
                p.append(z.copy())
    return p

def verification(L):
    n =len(L[0])
    s= [0]*n
    for j in L:
        for i,v in enumerate(j):
            s[i] += v
    return s

print(verification(permutlisteP(L)))

#%%
from itertools import permutations

for j in permutations([1,2,3,4]):
    print(j)


#%% " PAGE 353 "

def partiesliste(seq):
   p = []
   i, imax = 0, 2**len(seq)-1
   while i <= imax:
      s = []
      j, jmax = 0, len(seq)-1
      while j <= jmax:
         if (i>>j)&1 == 1:
           s.append(seq[j])
         j += 1
      p.append(s)
      i += 1
   return p

def partieschaine(ch):
   return[' '.join(z) for z in partiesliste(list(ch))]
   
#%%

from random import random

def Series():
    s = 1
    p = int (random()+0.5 )
    for _ in range(9):
        r = int( random()+0.5 )
        if r != p:
            s = s+1
        p = r
    return s

def Series2():
    s = 1
    p = int (random()+0.5 )
    L = [p]
    for _ in range(9):
        r = int( random()+0.5 )
        L.append(r)
        if r != p:
            s = s+1
        p = r
    return s,L

def SimulationSeries(N):
    return [ Series() for _ in range(N) ]

def moyenne(L):
    return sum(L)/len(L)

#%%
def Sont_orthogonales(A,B,C,D):
    """
    A,B,C,D sont les coordoonnées des points de l'espace sous de listes ou tuples
    retourne booléan
    """
    u = [ B[0]-A[0], B[1]-A[1], B[2]-A[2] ]
    v = [ D[0]-C[0], D[1]-C[1], D[2]-C[2] ]
    
    return u[0]*v[0] + u[1]*v[1] + u[2]*v[2] == 0

#Sont_orthogonales([1,1,3/4], [0,1/2,1], [1,0,-5/4], [1,3/4,1/4])
    
def Sont_orthogonales2(A,B,C,D,prec = 10**-8):
    """
    A,B,C,D sont les coordoonnées des points de l'espace sous de listes ou tuples
    retourne booléan
    """
    u = [ B[0]-A[0], B[1]-A[1], B[2]-A[2] ]
    v = [ D[0]-C[0], D[1]-C[1], D[2]-C[2] ]
    
    return abs(u[0]*v[0] + u[1]*v[1] + u[2]*v[2]) < prec

#Sont_orthogonales2( (1,1,3/4), (0,1/2,1), (1,0,-5/4), (1,3/4,1/4))
    
#%%
from mpmath import sqrt,mp,nstr,log10

mp.dps = 100


def archimede(n):
    S=2*sqrt(2)
    T=4
    for k in range(n):
        T=2*S*T/(S+T)
        S=sqrt(S*T)
        
    return T,S

S,T=archimede(100)

def affichage(S,T):
    # n nombre de chiffres commun entre S et T
    n = int(-log10(S-T))
    return nstr(S,n),n

print(affichage(S,T))