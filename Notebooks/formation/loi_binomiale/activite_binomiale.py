#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 18:43:34 2021

@author: Pascal Malingrey
formation 19 janv. 2021
"""

#%% ACTIVITÉ loi binomiale
from random import random 

def bernoulli(p):
    return int(random()+p)

def binomiale(n,p):
    """
    n: nombre d'épreuves
    p: probabilité de succès pour lors d'une épreuve
    """
    return sum([bernoulli(p) for i in range(n)])

def echantillon(N,n,p):
    """
    N: nombre d'échantillon
    n,p: paramètres de loi binomiale
    """
    return [binomiale(n,p) for _ in range(N)]

def frequences2(E,M):
    """
    E : listes de valeurs
    M: la plus grande des valeurs possibles
    """
    freq = []
    L = len(E)
    for i in range(M+1):
        freq.append(E.count(i)/L)
    return freq

def frequences(E,M):
    """
    E : listes de valeurs
    M: la plus grande des valeurs possibles
    """
    return [E.count(i)/len(E) for i in range(M+1)]
#%%

from matplotlib import pyplot as plt
from matplotlib import style
style.use('fivethirtyeight')

n, p, N = 15, 0.4, 300

def representation(n,p,N):
    serie = frequences(echantillon(N,n,p),n)
    x=[i for i in range(n+1)]
    plt.bar(x,serie,color='b' )
    

plt.figure(figsize=(12,6))
for i in range(1,4):
    plt.subplot(2,3,i)
    representation(n,p,N)


#%%

from scipy.stats import binom
x = [i for i in range(16)]
#plt.subplots(1, 1,figsize=(3.5,3))
plt.subplot(2,3,5)
plt.bar(x, binom.pmf(x,15,0.4))
plt.show()

    
